FROM openjdk:11.0.7-slim-buster
MAINTAINER Nanoo (arnaudlaval33@gmail.com)
ADD target/gateway-server.jar gateway-server.jar
ENTRYPOINT ["java","-jar","gateway-server.jar"]
